const totalSeat = parseInt(prompt('Nhập số lượng ghế'));
var isSpinSeat = false;
var interSeat;
/* const totalSeat = 10; */

function get_random_color_seat() {
    var color = "";
    for (var i = 0; i < 3; i++) {
        var sub = Math.floor(Math.random() * 256).toString(16);
        color += (sub.length == 1 ? "0" + sub : sub);
    }
    return "#" + color;
}

function generateSeat(totalSeat) {
    let segments = [];
    for (let i = 0; i < totalSeat; i++) {
        let infoSegment = {
            fillStyle: get_random_color_seat(),
            text: `${i + 1}`,
        };
        segments.push(infoSegment);
    }
    return segments;
}


// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters
// note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
// -------------------------------------------------------
function alertPrizeSeat(indicatedSegment) {
    // Do basic alert of the segment text.
    // You would probably want to do something more interesting with this information.
    document.getElementById('seat-result').innerHTML = indicatedSegment.text;
    document.getElementById('table-td').style.visibility = 'visible';
    // Swal.fire({
    //     position: 'top-end',
    //     icon: 'success',
    //     title: `Số ghế may mắn là: ${indicatedSegment.text}`,
    //     showConfirmButton: false,
    //     timer: 500
    // });
}
const fontSizeSeat = 20 + (Math.abs(totalSeat - 50) * (1 / 3));
let theWheelSeat = new Winwheel({
    'canvasId': 'canvasSeat',
    'numSegments': totalSeat, // Specify number of segments.
    'outerRadius': 220, // Set outer radius so wheel fits inside the background.
    'textFontSize': fontSizeSeat, // Set font size as desired.
    'fillStyle': '#7de6ef',
    'textAlignment': 'outer',
    'segments': generateSeat(totalSeat), // Define segments including colour and text. 
    'animation': // Specify the animation to use.
    {
        'type': 'spinToStop',
        'duration': Math.floor(Math.random() * (17 - 5 + 1)) + 10,
        'spins': Math.floor(Math.random() * (17 - 5 + 1)) + 15,
        'callbackFinished': alertPrizeSeat,
        'callbackSound': playSoundSeat, // Function to call when the tick sound is to be triggered.
        'soundTrigger': 'pin', // Specify pins are to trigger the sound, the other option is 'segment'.
        'easing': 'Power4.easeOut',
        'direction': 'clockwise',
    },
    'pins': {
        'number': totalSeat // Number of pins. They space evenly around the wheel.
    }
});



// -----------------------------------------------------------------
// This function is called when the segment under the prize pointer changes
// we can play a small tick sound here like you would expect on real prizewheels.
// -----------------------------------------------------------------
let audioSeat = new Audio('tick.mp3'); // Create audio object and load tick.mp3 file.

function playSoundSeat() {
    // Stop and rewind the sound if it already happens to be playing.
    audioSeat.pause();
    audioSeat.currentTime = 0;

    // Play the sound.
    audioSeat.play();
}

// =======================================================================================================================
// Code below for the power controls etc which is entirely optional. You can spin the wheel simply by
// calling theWheel.startAnimation();
// =======================================================================================================================
let wheelPowerSeat = 0;
let wheelSpinningSeat = false;

// -------------------------------------------------------
// Function to handle the onClick on the power buttons.
// -------------------------------------------------------
function powerSelectedSeat(powerLevel) {
    // Ensure that power can't be changed while wheel is spinning.
    if (wheelSpinningSeat == false) {
        // Reset all to grey incase this is not the first time the user has selected the power.
        document.getElementById('pw4').className = "";
        document.getElementById('pw5').className = "";
        /* document.getElementById('pw6').className = "";
 */
        // Now light up all cells below-and-including the one selected by changing the class.
        if (powerLevel >= 4) {
            document.getElementById('pw4').className = "pw4";
        }

        if (powerLevel >= 5) {
            document.getElementById('pw5').className = "pw5";
        }

        /* if (powerLevel >= 6) {
            document.getElementById('pw6').className = "pw6";
        } */

        // Set wheelPower var used when spin button is clicked.
        wheelPowerSeat = powerLevel;

        // Light up the spin button by changing it's source image and adding a clickable class to it.
        document.getElementById('spin_button_seat').src = "spin_on.png";
        document.getElementById('spin_button_seat').className = "clickable";
    }
}

// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
function startSpinSeat() {
    document.getElementById('table-td').style.visibility = 'hidden';
    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinningSeat == false) {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.
        if (wheelPowerSeat == 1) {
            theWheelSeat.animation.spins = 8;
        } else if (wheelPowerSeat == 2) {
            theWheelSeat.animation.spins = 15;
        } else if (wheelPowerSeat == 3) {
            theWheelSeat.animation.spins = 22;
        }

        // Disable the spin button so can't click again while wheel is spinning.
        document.getElementById('spin_button_seat').src = "spin_off.png";
        document.getElementById('spin_button_seat').className = "";

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheelSeat.startAnimation();

        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinningSeat = true;
    }

    if (isSpinTable === true) {
        return;
    } else {
        console.log(isSpinTable);
        isSpinSeat = true;
        interSeat = setInterval(openModalSeat, 3500);
        function openModalSeat() {
            console.log('seat interval');
            const seat = document.getElementById('seat-result');
            const table = document.getElementById('table-result');
            if (seat.innerHTML !== "0" && table.innerHTML !== "0") {
                const seatConverted = parseInt(seat.innerHTML, 10) < 10 ? `0${seat.innerHTML}` : seat.innerHTML;
                const tableConverted = parseInt(table.innerHTML, 10) < 10 ? `0${table.innerHTML}` : table.innerHTML;
                audioCongratulations.pause();
                audioCongratulations.currentTime = 0;
                // Play the sound.
                audioCongratulations.play();
                Swal.fire({
                    title: `<span style="font-size: 38px">Số may mắn là: ${tableConverted}${seatConverted}</span>`,
                    width: 600,
                    padding: '3em',
                    background: '#fff url("./trees.png")',
                    backdrop: `
                                rgba(0,0,123,0.4)
                                url("./nyan-cat.gif")
                                left top
                                no-repeat
                            `,
                    onClose: () => {
                        seat.innerHTML = 0;
                        table.innerHTML = 0;
                        resetWheelTable();
                        resetWheelSeat();
                        audioCongratulations.currentTime = 0;
                        audioCongratulations.pause();
                    }
                });
                clearInterval(interSeat);
            }
        }
    }
}

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
function resetWheelSeat() {
    theWheelSeat.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
    theWheelSeat.rotationAngle = 0; // Re-set the wheel angle to 0 degrees.
    theWheelSeat.draw(); // Call draw to render changes to the wheel.
    isSpinSeat = false;
    clearInterval(interSeat)
    document.getElementById('pw4').className = ""; // Remove all colours from the power level indicators.
    document.getElementById('pw5').className = "";
    document.getElementById('seat-result').innerHTML = 0;
    /* document.getElementById('pw6').className = ""; */
    document.getElementById('spin_button_seat').src = "spin_on.png";
    document.getElementById('spin_button_seat').className = "clickable";
    document.getElementById('table-td').style.visibility = 'visible';
    wheelSpinningSeat = false; // Reset to false to power buttons and spin can be clicked again.
}