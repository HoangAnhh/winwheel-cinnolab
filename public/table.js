const audioCongratulations = new Audio('congratulations.mp3');
const totalTable = parseInt(prompt('Nhập số lượng bàn'));
var isSpinTable = false;
var interTable;
/* const totalTable = 10; */

function get_random_color_table() {
    var color = "";
    for (var i = 0; i < 3; i++) {
        var sub = Math.floor(Math.random() * 256).toString(16);
        color += (sub.length == 1 ? "0" + sub : sub);
    }
    return "#" + color;
}

function generateTable(totalTable) {
    let segments = [];
    for (let i = 0; i < totalTable; i++) {
        let infoSegment = {
            fillStyle: get_random_color_table(),
            text: `${i + 1}`,
        };
        segments.push(infoSegment);
    }
    return segments;
}

// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters
// note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
// -------------------------------------------------------
function alertPrizeTable(indicatedSegment) {
    // Do basic alert of the segment text.
    // You would probably want to do something more interesting with this information.
    document.getElementById('table-result').innerHTML = indicatedSegment.text;
    document.getElementById('seat-td').style.visibility = 'visible';
    // Swal.fire({
    //     position: 'top-end',
    //     icon: 'success',
    //     title: `Số bàn may mắn là: ${indicatedSegment.text}`,
    //     showConfirmButton: false,
    //     timer: 1500
    // });
}

const fontSizeTable = 20 + (Math.abs(totalTable - 50) * (1 / 3));
let theWheelTable = new Winwheel({
    'canvasId': 'canvasTable',
    'numSegments': totalTable, // Specify number of segments.
    'outerRadius': 220, // Set outer radius so wheel fits inside the background.
    'textFontSize': fontSizeTable, // Set font size as desired.
    'fillStyle': '#7de6ef',
    'textAlignment': 'outer',
    'segments': generateTable(totalTable), // Define segments including colour and text. 
    'animation': // Specify the animation to use.
    {
        'type': 'spinToStop',
        'duration': Math.floor(Math.random() * (17 - 5 + 1)) + 20,
        'spins': Math.floor(Math.random() * (17 - 5 + 1)) + 10,
        'callbackFinished': alertPrizeTable,
        'callbackSound': playSoundTable, // Function to call when the tick sound is to be triggered.
        'soundTrigger': 'pin', // Specify pins are to trigger the sound, the other option is 'segment'.
        'easing': 'Power4.easeOut',
        'direction': 'clockwise',
    },
    'pins': {
        'number': totalTable // Number of pins. They space evenly around the wheel.
    }
});



// -----------------------------------------------------------------
// This function is called when the segment under the prize pointer changes
// we can play a small tick sound here like you would expect on real prizewheels.
// -----------------------------------------------------------------
let audioTable = new Audio('tick.mp3'); // Create audio object and load tick.mp3 file.

function playSoundTable() {
    // Stop and rewind the sound if it already happens to be playing.
    audioTable.pause();
    audioTable.currentTime = 0;

    // Play the sound.
    audioTable.play();
}

// =======================================================================================================================
// Code below for the power controls etc which is entirely optional. You can spin the wheel simply by
// calling theWheel.startAnimation();
// =======================================================================================================================
let wheelPowerTable = 0;
let wheelSpinningTable = false;

// -------------------------------------------------------
// Function to handle the onClick on the power buttons.
// -------------------------------------------------------
function powerSelectedTable(powerLevel) {
    // Ensure that power can't be changed while wheel is spinning.
    if (wheelSpinningTable == false) {
        // Reset all to grey incase this is not the first time the user has selected the power.
        document.getElementById('pw1').className = "";
        document.getElementById('pw2').className = "";
        /* document.getElementById('pw3').className = ""; */

        // Now light up all cells below-and-including the one selected by changing the class.
        if (powerLevel >= 1) {
            document.getElementById('pw1').className = "pw1";
        }

        if (powerLevel >= 2) {
            document.getElementById('pw2').className = "pw2";
        }

        /* if (powerLevel >= 3) {
            document.getElementById('pw3').className = "pw3";
        } */

        // Set wheelPower var used when spin button is clicked.
        wheelPowerTable = powerLevel;

        // Light up the spin button by changing it's source image and adding a clickable class to it.
        document.getElementById('spin_button_table').src = "spin_on.png";
        document.getElementById('spin_button_table').className = "clickable";
    }
}

// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
function startSpinTable() {
    document.getElementById('seat-td').style.visibility = 'hidden';
    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinningTable == false) {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.
        if (wheelPowerTable == 1) {
            theWheelTable.animation.spins = 8;
        } else if (wheelPowerTable == 2) {
            theWheelTable.animation.spins = 15;
        } else if (wheelPowerTable == 3) {
            theWheelTable.animation.spins = 22;
        }

        // Disable the spin button so can't click again while wheel is spinning.
        document.getElementById('spin_button_table').src = "spin_off.png";
        document.getElementById('spin_button_table').className = "";

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheelTable.startAnimation();

        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinningTable = true;
    }

    if (isSpinSeat === true) {
        return;
    } else {
        console.log(isSpinSeat);
        isSpinTable = true;
        interTable = setInterval(openModalTable, 3500);
        
        function openModalTable() {
            console.log('table interval');
            const seat = document.getElementById('seat-result');
            const table = document.getElementById('table-result');
            if (seat.innerHTML !== "0" && table.innerHTML !== "0") {
                const seatConverted = parseInt(seat.innerHTML, 10) < 10 ? `0${seat.innerHTML}` : seat.innerHTML;
                const tableConverted = parseInt(table.innerHTML, 10) < 10 ? `0${table.innerHTML}` : table.innerHTML;
                audioCongratulations.pause();
                audioCongratulations.currentTime = 0;
                // Play the sound.
                audioCongratulations.play();
                Swal.fire({
                    title: `<span style="font-size: 38px">Số may mắn là: ${tableConverted}${seatConverted}</span>`,
                    width: 600,
                    padding: '3em',
                    background: '#fff url("./trees.png")',
                    backdrop: `
                                rgba(0,0,123,0.4)
                                url("./nyan-cat.gif")
                                left top
                                no-repeat
                            `,
                    onClose: () => {
                        seat.innerHTML = 0;
                        table.innerHTML = 0;
                        resetWheelTable();
                        resetWheelSeat();
                        audioCongratulations.currentTime = 0;
                        audioCongratulations.pause();
                    }
                });
                clearInterval(interTable);
            }
        }
    }
}

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
function resetWheelTable() {
    theWheelTable.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
    theWheelTable.rotationAngle = 0; // Re-set the wheel angle to 0 degrees.
    theWheelTable.draw(); // Call draw to render changes to the wheel.
    isSpinTable = false;
    clearInterval(interTable);
    document.getElementById('pw1').className = ""; // Remove all colours from the power level indicators.
    document.getElementById('pw2').className = "";
    document.getElementById('table-result').innerHTML = 0;
    /* document.getElementById('pw3').className = ""; */
    document.getElementById('spin_button_table').src = "spin_on.png";
    document.getElementById('spin_button_table').className = "clickable";
    document.getElementById('seat-td').style.visibility = 'visible';
    wheelSpinningTable = false; // Reset to false to power buttons and spin can be clicked again.
}